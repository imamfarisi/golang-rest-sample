package main

import (
	"log"
	"net"

	"google.golang.org/grpc"
	dao "lawencon.com/app1/dao/impl"
	"lawencon.com/app1/db"
	"lawencon.com/app1/grpc/controller"
)

func _main() {
	db := db.NewDb()
	dao.InitDb(db)

	lis, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	controller.SetServer(grpcServer)
	grpcServer.Serve(lis)
}
