package security

import (
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v4"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"lawencon.com/app1/constant"
	"lawencon.com/app1/dto"
	baseService "lawencon.com/app1/service/impl"
)

type JwtClaims struct {
	Id uint `json:"id"`
	jwt.RegisteredClaims
}

func GenerateToken(id uint) (string, error) {
	claims := &JwtClaims{
		id,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 24)),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString([]byte(constant.SECRET_KEY))

	if err != nil {
		return "", err
	}
	return t, nil
}

func JwtMiddlewareFunc() echo.MiddlewareFunc {
	configJwt := echojwt.Config{
		NewClaimsFunc: func(c echo.Context) jwt.Claims {
			return new(JwtClaims)
		},
		SigningKey: []byte(constant.SECRET_KEY),
		ErrorHandler: func(c echo.Context, e error) error {
			return c.JSON(http.StatusUnauthorized, dto.CommonRes{Message: e.Error()})
		},
		SuccessHandler: func(c echo.Context) {
			user := c.Get("user").(*jwt.Token)
			claims := user.Claims.(*JwtClaims)
			baseService.SetLoginId(claims.Id)
		},
	}

	return echojwt.WithConfig(configJwt)
}
