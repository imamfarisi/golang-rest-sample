package impl

import (
	movieDaoImpl "lawencon.com/app1/dao/impl"
	"lawencon.com/app1/dto"
	"lawencon.com/app1/model"
)

type MovieServiceImpl struct{}

var movieDao = movieDaoImpl.NewMovieDaoImpl()

func NewMovieServiceImpl() MovieServiceImpl {
	return MovieServiceImpl{}
}

func (MovieServiceImpl) InsertData(movieDto dto.MovieInsertReq) error {
	var movie = model.Movie{}
	movie.MovieName = movieDto.MovieName
	movie.CategoryId = movieDto.CategoryId
	movie.CreatedBy = loginId

	return movieDao.InsertMovie(movie)
}
