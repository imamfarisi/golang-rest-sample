package service

import "lawencon.com/app1/dto"

type MovieService interface {
	InsertData(movieDto dto.MovieInsertReq) error
}
