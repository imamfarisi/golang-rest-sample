package controller

import (
	"context"
	"log"
	"strings"

	"github.com/golang-jwt/jwt/v4"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"lawencon.com/app1/constant"
	"lawencon.com/app1/security"
)

func SetServer(grpcServer *grpc.Server) {
	initLoginServiceServer(grpcServer)
	initMovieServiceServer(grpcServer)
}

func validToken(c context.Context) error {
	var token = ""
	if md, ok := metadata.FromIncomingContext(c); ok {
		token = md.Get("Authorization")[0]
	}

	token = strings.Split(token, " ")[1]

	_, err := jwt.ParseWithClaims(token, &security.JwtClaims{}, func(t *jwt.Token) (interface{}, error) {
		return []byte(constant.SECRET_KEY), nil
	})

	if err != nil {
		log.Println("Error =>", err.Error())
		return err
	}

	return nil
}
