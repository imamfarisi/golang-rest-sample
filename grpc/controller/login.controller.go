package controller

import (
	"context"

	"google.golang.org/grpc"
	proto "lawencon.com/app1/grpc/proto"
	pb "lawencon.com/app1/grpc/proto/login"
	"lawencon.com/app1/security"
)

type LoginServiceServer struct {
	pb.UnimplementedLoginServiceServer
}

func initLoginServiceServer(grpcServer *grpc.Server) {
	pb.RegisterLoginServiceServer(grpcServer, &LoginServiceServer{})
}

func (c *LoginServiceServer) Login(ctx context.Context, in *pb.LoginReq) (*proto.CommonRes, error) {
	//get user by email and pass here
	var userId uint = 1
	token, err := security.GenerateToken(userId)
	if err != nil {
		return &proto.CommonRes{}, err
	}

	return &proto.CommonRes{Message: token}, nil
}
