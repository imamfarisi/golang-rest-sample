package controller

import (
	"context"

	"google.golang.org/grpc"
	"lawencon.com/app1/dto"
	"lawencon.com/app1/grpc/proto"
	pb "lawencon.com/app1/grpc/proto/movie"
	movieServiceImpl "lawencon.com/app1/service/impl"
)

var movieService = movieServiceImpl.NewMovieServiceImpl()

type MovieServiceServer struct {
	pb.UnimplementedMovieServiceServer
	ValidAuth bool
}

func initMovieServiceServer(grpcServer *grpc.Server) {
	pb.RegisterMovieServiceServer(grpcServer, &MovieServiceServer{})
}

func (m *MovieServiceServer) InsertData(c context.Context, data *pb.MovieInsertReq) (*proto.CommonRes, error) {
	err := validToken(c)

	if err != nil {
		return &proto.CommonRes{}, err
	}

	movieDto := dto.MovieInsertReq{
		MovieName:  data.MovieName,
		CategoryId: uint(data.CategoryId),
	}

	if err := movieService.InsertData(movieDto); err != nil {
		return &proto.CommonRes{}, err
	}

	return &proto.CommonRes{Message: "Inserted"}, nil
}
