package model

type Movie struct {
	BaseModel
	MovieName     string        `gorm:"not null;size:30"`
	CategoryId    uint          `gorm:"not null"`
	MovieCategory MovieCategory `gorm:"foreignKey:CategoryId"`
}

func (Movie) TableName() string {
	return "t_movie"
}
