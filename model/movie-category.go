package model

type MovieCategory struct {
	BaseModel
	CategoryCode string `gorm:"unique;not null;size:3"`
	CategoryName string `gorm:"not null;size:30"`
}

func (MovieCategory) TableName() string {
	return "t_movie_category"
}
