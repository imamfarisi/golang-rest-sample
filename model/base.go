package model

import (
	"database/sql"
	"time"

	"gorm.io/gorm"
)

type BaseModel struct {
	Id        uint          `gorm:"primaryKey"`
	CreatedBy uint          `gorm:"<-:create"`
	CreatedAt time.Time     `gorm:"<-:create"`
	UpdatedBy sql.NullInt64 `gorm:"<-:update"`
	UpdatedAt sql.NullTime  `gorm:"<-:update"`
}

func (u *BaseModel) BeforeCreate(tx *gorm.DB) error {
	u.CreatedAt = time.Now()
	return nil
}
