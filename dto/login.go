package dto

type LoginReq struct {
	Email string `json:"email"`
	Pass  string `json:"pass"`
}
