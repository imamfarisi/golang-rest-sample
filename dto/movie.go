package dto

type MovieInsertReq struct {
	MovieName  string `json:"movieName"`
	CategoryId uint   `json:"categoryId"`
}
