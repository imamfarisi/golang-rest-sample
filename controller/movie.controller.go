package controller

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"lawencon.com/app1/dto"
	"lawencon.com/app1/model"
	"lawencon.com/app1/security"
	movieServiceImpl "lawencon.com/app1/service/impl"
)

var movieService = movieServiceImpl.NewMovieServiceImpl()

func initMovies(server *echo.Echo) {
	movies := server.Group("/movies")
	movies.Use(security.JwtMiddlewareFunc())
	movies.GET("", getAllData)
	movies.POST("", saveData)
}

func getAllData(c echo.Context) error {
	var data = []model.Movie{}
	fmt.Println("getAllData")
	return c.JSON(http.StatusOK, data)
}

func saveData(c echo.Context) error {
	var movie dto.MovieInsertReq
	if err := c.Bind(&movie); err != nil {
		return badRequestRes(c, err.Error())
	}

	err := movieService.InsertData(movie)
	if err != nil {
		return badRequestRes(c, err.Error())
	}

	res := dto.CommonRes{Message: "Inserted"}
	return c.JSON(http.StatusCreated, res)
}
