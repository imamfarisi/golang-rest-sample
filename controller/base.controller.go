package controller

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"lawencon.com/app1/dto"
)

func InitRoutes(server *echo.Echo) {
	initLogin(server)
	initMovies(server)
}

func badRequestRes(c echo.Context, data string) error {
	return c.JSON(http.StatusBadRequest, dto.CommonRes{Message: data})
}

func unAuthRequestRes(c echo.Context, data string) error {
	return c.JSON(http.StatusUnauthorized, dto.CommonRes{Message: data})
}
