package controller

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"lawencon.com/app1/dto"
	"lawencon.com/app1/security"
)

func initLogin(server *echo.Echo) {
	server.POST("login", login)
}

func login(c echo.Context) error {
	var loginReq dto.LoginReq
	c.Bind(&loginReq)

	//get user by email and pass here
	var userId uint = 1
	token, err := security.GenerateToken(userId)
	if err != nil {
		return unAuthRequestRes(c, "Invalid Login")
	}

	return c.JSON(http.StatusOK, dto.CommonRes{Message: token})
}
