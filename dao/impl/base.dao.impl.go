package impl

import "gorm.io/gorm"

var db *gorm.DB

func InitDb(gormDb *gorm.DB) {
	db = gormDb
}
