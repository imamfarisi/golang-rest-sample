package dao

import "lawencon.com/app1/model"

type MovieDao interface {
	InsertMovie(movie model.Movie) error
}
