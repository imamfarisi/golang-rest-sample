package main

import (
	"lawencon.com/app1/controller"
	dao "lawencon.com/app1/dao/impl"
	"lawencon.com/app1/db"
	"lawencon.com/app1/server"
)

func main() {
	server := server.NewServer()
	db := db.NewDb()

	controller.InitRoutes(server)
	dao.InitDb(db)

	server.Logger.Fatal(server.Start(":1234"))
}
